
/**
 * Write a description of class VarStorage here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class VarStorage
{
    private int p1_score;
    private int p2score;
    
    private String p1_tag;
    private String p1_prefix;
    
    private String p2_prefix;
    private String p2_tag;
    
    private String label;
    private String misc;
    
    public VarStorage() {
        p1_score = 0;
        p2score = 0;
    }
    
    public int getP1Score() {
        return p1_score;
    }
    
    public void setP1Score(int score) {
        p1_score = score;
    }
    
    public int getP2Score() {
        return p2score;
    }
    
    public void setP2Score(int score) {
        p2score = score;
    }
    
    public String getP1Prefix() {
        return p1_prefix;
    }
    
    public void setP1Prefix(String p) {
        p1_prefix = p;
    }
    
    public String getP1Tag() {
        return p1_tag;
    }
    
    public void setP1Tag(String t) {
        p1_tag = t;
    }
    
    public String getP2Prefix() {
        return p2_prefix;
    }
    
    public void setP2Prefix(String p) {
        p2_prefix = p;
    }
    
    public String getP2Tag() {
        return p2_tag;
    }
    
    public void setP2Tag(String t) {
        p2_tag = t;
    }
    
    public String getLabel() {
        return label;
    }
    
    public void setLabel(String l) {
        label = l;
    }
    
    public String getMisc() {
        return misc;
    }
    
    public void setMisc(String m) {
        misc = m;
    }
    
    public void reset() {
        setP1Score(0);
        setP2Score(0);
        setP1Prefix("");
        setP1Tag("");
        setP2Prefix("");
        setP2Tag("");
        setLabel("");
        setMisc("");
    }
}
